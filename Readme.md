# Simple traefik demo

````
docker-compose up -d
````


## To try on localhost

Add domain to `/etc/hosts` or (`/private/etc/hosts` on mac)
````
127.0.0.1 whoami.test
127.0.0.1 whoami2.test
127.0.0.1 whoami3.test
````